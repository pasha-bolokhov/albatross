# To do list


## Authentication Service

* Retain the logged in state over reloads

* Implement `CanDeactivate` guard if necessary


## Authentication menu

* Promote the auth buttons component into its own (eagerly-loaded) module, and move the "secure" router-outlet into that module

* In this case, however, `mat-menu-item`s are not going to be direct descendants of `mat-menu` — need to see how to make this work

* Potentially, Sign Up process can request more information from the user, and will need to designate a separate page for it, instead of a dialogue

* Process potential errors from `signInAnonymously()` and display a `MatSnackBar` or use another strategy to indicate of an error

* Function `signInAnonymously()` should perform authentication itself, before closing the dialogue, and display an error if it occurs

* Use a spinner for authentication actions

* On mobile devices show the tooltips below the buttons

* GitHub sign in method does not provide `displayName`, and maybe `read:user` scope is not needed


## Styling

* Ensure a good background image exists

* Style the snack bars


## Colour theme

* Design own theme

* Derive colours marked with AAAA from the theme


## Toolbar

* Highlight the active page


## Landing Page

* Make the splash dialogue window appear gradually

* Fix the scrollbars of the dialog window on mobile devices


## Dialogues

* Design a colour style for the dialogues

* Fix the scrollbars in the dialogues on mobile devices


## Packages

* Add a "back" button at the top of the preview page


## PWA

* Provide the icons of all necessary sizes, including `apple-touch-icon`


## Server Side Rendering

* Enable Server Side Rendering

* Ensure the existence of a correct App Shell


## Functions

* Selecting a package should create or update the "user-packages" collection in Firebase

* Creating a user should create an empty list in the corresponding "user-packages" document

* Add another model for `Cruise` in analogy to `Hotel`

* A separate CI/CD deployment may be needed for functions

* Test functions locally
