# Albatross

Albatross Travel Ltd (TM) is a web application that showcases a simplified fictional travel agency store-front, allowing the clients to select and purchase vacation trips around the world. This project is a revamped version of the web aplication which originated as a Computer Science course project at Computer Systems Technology program at Camosun College in the academic years 2014/2015. This web application is a fresh start 2020 project, using serverless approach based and hosted on Google Firebase

## Construction
The main purpose of this project is showcasing the modern features of web apps. The project is currently fully under the migration stage from its original AngularJS/MySQL implementation to the more modern techniques. The techniques, services and features include
  *  __Angular__
  *  __Differential Loading__ (automatic with Angular)
  *  __Routing, Guards__
  *  __Lazy Loading__
  *  __Material Design__ with a styling touch
  *  __Firestore__
  *  __Firestore Rules__
  *  __Firebase Auth__
  *  __Federated Identity Provider-based login__
  *  __Firebase Hosting__
  *  __Firebase Storage__
  *  __Cloud Functions__
  *  Server-side rendering
  *  Accelerated Mobile Pages
  *  Search Engine Optimization
  *  Internationalization (i18n)
  *  __PWA__
  * Unit Testing
  * End-to-end testing
  *  PayPal check-out

The bold-accented features are the ones that are already in use in the app

## Original Authors
Candise Wang<br>
Toshiyasu Azakawa<br>
Pasha Bolokhov
