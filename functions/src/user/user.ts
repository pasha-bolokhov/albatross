/**
 * @fileoverview Functions for processing authentication events.
 */
import * as functions from 'firebase-functions';

import { db } from '../init';
import { deleteCollection } from '../util';

const DELETE_BATCH_SIZE: number = 100;

/**
 * Processes creation of a new user.
 *
 * Currently just a stub.
 *
 * @param change            The change that triggered this function
 * @param context           The context of the change
 */
export const onUserCreate = functions.auth.user().onCreate(async (user): Promise<any> => {
  await db.doc(`users/${user.uid}`).set({
    'uid': user.uid,
    'status': 'confirmed'
  }, {merge: true});
  return null;
});

/**
 * Processes deletion of a user.
 *
 * Deletes the corresponding document in the 'users' collection.
 * Note that we are only processing one level of subcollections.
 *
 * @param change            The change that triggered this function
 * @param context           The context of the change
 */
export const onUserDelete = functions.auth.user().onDelete(async (user): Promise<any> => {
  await deleteCollection(db, `users/${user.uid}/cart`, DELETE_BATCH_SIZE);
  await deleteCollection(db, `users/${user.uid}/cartSubmissions`, DELETE_BATCH_SIZE);
  await db.doc(`users/${user.uid}`).delete();
  return null;
});
