/**
 * @file Information needed for submitting an item to the cart
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { RequiredKeys } from "../../defs";

/**
 * The type for an error indication for a submission
 */
export type CartSubmissionErrorType = "application-error" | "internal-error" | "package-not-available";

/**
 * The errors that we throw explicitly
 */
export class SubmissionError extends Error {
  constructor(message?: CartSubmissionErrorType) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
  get message(): CartSubmissionErrorType {
    return this.message;
  }
}

/**
 * The type for the completion status of a submission
 */
export type CartSubmissionStatus = "success" | CartSubmissionErrorType;

export interface CartSubmission {
  /**
   * The ID of the item that is being submitted.
   * @type string
   */
  packageId: string;

  /**
   * The status of processing of the item.
   * Added by Functions
   * @type CartSubmissionStatus
   */
  status?: CartSubmissionStatus;
}

/**
 * A type guard for interface CartSubmission
 *
 * @param   {any}       candidate               An object to check for compliance with CartSubmission
 *
 * @returns {boolean}                           Whether 'candidate' is an instance of CartSubmission
 */
export function isCartSubmission(candidate: any): candidate is CartSubmission {
  if (candidate === null || candidate === undefined) {
    return false;
  }

  const keys: Record<RequiredKeys<CartSubmission>, true> = {
    packageId: true,
  };

  for (const key in keys) {
    if (candidate[key] === undefined) {
      return false;
    }
  }

  return true;
}
