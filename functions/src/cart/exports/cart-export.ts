/**
 * @file The definition of the cart item interface
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { Package } from "../../packages/models/package";
import { RequiredKeys } from "../../defs";

/**
 * Cart item information to send to the Front End
 */
export type CartExport = Pick<Package, "destination" | "imageUrl" | "name" | "price"> & {
  /**
   * Time the item was added to the cart.
   * @type Date
   */
  createdAt: Date;

  /**
   * Quantity to purchase.
   * @type number
   */
  quantity: number;

  /**
   * Total price for this cart item
   */
  totalPrice: number;

  /**
   * Time the item was updated in the cart.
   * @type Date
   */
  updatedAt?: Date;
};

/**
 * A type guard for interface Export
 *
 * @param   {any}       candidate               The object to check for compliance with CartExport
 *
 * @returns {boolean}                           Whether 'candidate' is an instance of CartExport
 */
export function isCartExport(candidate: any): candidate is CartExport {
  if (candidate === null || candidate === undefined) {
    return false;
  }

  const keys: Record<RequiredKeys<CartExport>, true> = {
    createdAt: true,
    destination: true,
    imageUrl: true,
    name: true,
    price: true,
    quantity: true,
    totalPrice: true,
  };

  for (const key in keys) {
    if (candidate[key] === undefined) {
      return false;
    }
  }

  return true;
}
