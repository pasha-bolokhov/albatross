/**
 * @file This file processes submissions to the 'cartSubmissions' collection
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import { db } from '../init';
import { CartExport, isCartExport } from './exports/cart-export';
import { CartSubmission, CartSubmissionStatus, isCartSubmission, SubmissionError } from './models/cart-submission';
import { isPackage, Package } from '../packages/models/package';

/**
 * Processes a new document in 'cartSubmissions' collection, and removes it after completion
 *
 * @param   snapshot                            The snapshot of the created document
 * @param   context                             The context of the document
 */
export const onCartSubmission = functions.firestore.document('/users/{uid}/cartSubmissions/{docID}').onCreate(async (snapshot, context): Promise<any> => {
  try {
    const uid: string = context.params.uid;

    return await db.runTransaction(async (transaction: FirebaseFirestore.Transaction) => {
      let status: CartSubmissionStatus = "success";
      try {
        // Get the cart submission
        const submissionCandidate = snapshot.data();
        if (!isCartSubmission(submissionCandidate)) {
          throw new SubmissionError("application-error");
        }
        const submission: CartSubmission = submissionCandidate;
        const packageId: string = submission.packageId;

        // Get the possibly existing cart item
        const itemRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData> = db.doc(`/users/${uid}/cart/${packageId}`);
        const itemSnapshot: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData> = await transaction.get(itemRef);
        const existingExportCandidate = itemSnapshot.data();
        if (existingExportCandidate !== undefined && !isCartExport(existingExportCandidate)) {
          throw new SubmissionError("internal-error");
        }
        const existingExport: CartExport | undefined = existingExportCandidate;

        // Check the availability of the package
        const requestedQuantity: number = (existingExport === undefined ? 0 : existingExport.quantity) + 1;
        const packageRef: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData> = db.doc(`/packages/${packageId}`);
        const packageSnapshot: FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData> = await transaction.get(packageRef);
        const packageCandidate = packageSnapshot.data();
        if (!isPackage(packageCandidate)) {
          throw new SubmissionError("internal-error");
        }
        const packageData: Package = packageCandidate;
        if (packageData.available < requestedQuantity) {
          throw new SubmissionError("package-not-available");
        }

        // Form and store the export object
        const newData = {                         // The set of data that will override the existing item data
            destination: packageData.destination,
            imageUrl: packageData.imageUrl,
            name: packageData.name,
            price: packageData.price,
            quantity: admin.firestore.FieldValue.increment(1) as unknown as number,
            totalPrice: packageData.price * requestedQuantity,
          };
        const cartExport: CartExport = existingExport === undefined ? {
          ...newData,
          createdAt: admin.firestore.FieldValue.serverTimestamp() as unknown as Date,
        } : {
          ...existingExport,
          ...newData,
          updatedAt: admin.firestore.FieldValue.serverTimestamp() as unknown as Date,
        };

        transaction.set(itemRef, cartExport, {merge: true});
      } catch (error) {
        // Return an "internal-error" message for all errors that we do not throw
        status = (error instanceof SubmissionError) ? error.message : "internal-error";
      }

      // Set the status field in the cart submission object
      return transaction.set(snapshot.ref, {status}, {merge: true});
    });
  } catch (transactionError) {
    console.log('Transaction:', transactionError);
    return null;
  }
});
