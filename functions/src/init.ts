/**
 * Initialize Firebase admin and provide a 'db' shorthand for the Firestore
 */
import * as admin from 'firebase-admin';

admin.initializeApp();
export const db = admin.firestore();
