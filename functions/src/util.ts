/**
 * @file Utility functions and definitions
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */

/**
 * Deletes a collection of documents, splitting the operation into batches
 *
 * @param   {Firestore}                 db                            The database to operate upon
 * @param   {string}                    collectionPath                The path to the collection to delete
 * @param   {number}                    batchSize                     The size of batches
 */
export async function deleteCollection(db: FirebaseFirestore.Firestore, collectionPath: string, batchSize: number): Promise<void> {
  const collectionRef: FirebaseFirestore.CollectionReference<FirebaseFirestore.DocumentData> = db.collection(collectionPath);
  const query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData> = collectionRef.orderBy('__name__').limit(batchSize);

  return new Promise((resolve, reject) => {
    _deleteQueryBatch(db, query, resolve).catch(reject);
  });
}

/**
 * Performs a single batch of delete operations. For internal use
 *
 * @param   {Firestore}                 db                            The database to operate upon
 * @param   {Query}                     query                         The query used to list the documents
 * @param   {<function>}                resolve                       The function to be called to indicate success of overall operation
 */
async function _deleteQueryBatch(db: FirebaseFirestore.Firestore, query: FirebaseFirestore.Query<FirebaseFirestore.DocumentData>, resolve: (value?: void | PromiseLike<void> | undefined) => void): Promise <void> {
  const snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData> = await query.get();

  const batchSize: number = snapshot.size;
  if (batchSize === 0) {
    // When there are no documents left, we are done
    resolve();
    return;
  }

  // Delete documents in a batch
  const batch: FirebaseFirestore.WriteBatch = db.batch();
  snapshot.docs.forEach((doc: FirebaseFirestore.QueryDocumentSnapshot<FirebaseFirestore.DocumentData>) => {
    batch.delete(doc.ref);
  });
  await batch.commit();

  // Recurse on the next process tick, to avoid
  // exploding the stack.
  process.nextTick(async () => {
    await _deleteQueryBatch(db, query, resolve);
  });
}
