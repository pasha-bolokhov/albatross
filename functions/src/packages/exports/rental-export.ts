import { Rental } from '../models/rental';
import { SegmentNode } from './segment-node';

/**
 * Rental information to send to the Front End
 */
export interface RentalExport extends SegmentNode {
  /**
   * The type of node.
   * @type "rental"
   */
  nodeType: "rental";

  /**
   * The type of the rental
   * @type Rental
   */
  rentalType: Rental;
}
