import { Flight } from '../models/flight';
import { SegmentNode } from './segment-node';

/**
 * Flight information for export to the Front End
 */
export interface FlightExport extends Flight, SegmentNode {
  /**
   * Node type.
   * @type "flight+" | "flightInformation"
   */
  nodeType: "flight+" | "flightInformation";
}
