import { Hotel } from '../models/hotel';
import { SegmentNode } from './segment-node';

/**
 * Hotel display information for export to the Front End
 */
export interface HotelExport extends SegmentNode {
  /**
   * The display name of the hotel.
   * @type string
   */
  name: string;

  /**
   * Node type.
   * @type "hotel+"
   */
  nodeType: "hotel+";

  /**
   * Length of stay at the hotel.
   * @type number
   */
  duration: number;
}

export interface HotelInformationExport extends Hotel, SegmentNode {
    /**
   * The display name of the hotel.
   * @type string
   */
  name: string;

  /**
   * Node type.
   * @type "hotelInformation"
   */
  nodeType: "hotelInformation";
}
