import { Activity } from "../models/activity";
import { SegmentNode } from "./segment-node";

/**
 * The activity type for export to the Front End
 * The undefined value is a notification of an error
 */
export type ActivityType = Activity | undefined;

/**
 * Activity information for export to the Front End
 */
export interface ActivityExport extends SegmentNode {
  /**
   * The activity of the segment.
   * @type ActivityType
   */
  activity: ActivityType;

  /**
   * The location of the activity.
   * @type string | undefined
   */
  location: string | undefined;

  /**
   * Node type.
   * @type "activity+"
   */
  nodeType: "activity+";
}
