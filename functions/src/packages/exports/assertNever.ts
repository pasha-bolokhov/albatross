/**
 * Function used to fail compilation if the execution branch is able to reach a forbidden end point
 *
 * @param arg   never   Any object that should not exist at a given execution branch
 */
export function assertNever(arg: never): never {
  throw new Error("assertNever(): execution should not have reached here");
}
