/**
 * The subtype of node which defines non-expandable transport types
 */
export type FlatTransportType = "bike" | "boat" | "bus" | "ferry" | "train";

/**
 * The definition of the node type
 *
 * Node types with a plus "+" symbol indicate that child nodes with additional information are present
 */
export type NodeType = "activity+" |
                        "flight+" | "flightInformation" |
                        FlatTransportType |
                        "hotel+" | "hotelInformation" |
                        "rental" |
                        undefined;

/**
 * The definition of a node in the Segment tree
 */
export interface SegmentNode {
  /**
   * The type of this node.
   * @type NodeType
   */
  nodeType: NodeType;

  /**
   * Sub-nodes of this node.
   * Optional.
   * @type SegmentNode[]
   */
  children?: SegmentNode[];
}
