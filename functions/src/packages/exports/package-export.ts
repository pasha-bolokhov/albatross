import { Package } from "../models/package";

/**
 * Package information to send to the Front End
 */
export interface PackageExport extends Package {
  /**
   * The document ID of the package.
   * @type string
   */
  id: string;
}
