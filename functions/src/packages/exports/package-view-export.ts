import { Package } from "../models/package";
import { SegmentNode } from "./segment-node";

/**
 * Compound package information to send to the Front End
 */
export interface PackageViewExport extends Package {
  /**
   * The document ID of the package.
   * @type string
   */
  id: string;

  /**
   * The array of segments comprising the package.
   * @type SegmentNode[]
   */
  segments: SegmentNode[];
}
