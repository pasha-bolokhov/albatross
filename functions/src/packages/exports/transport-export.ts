import { SegmentNode } from "./segment-node";

/**
 * Transport information for export to the Front End
 * Used collectively for transport types other than "flight"
 */
export interface TransportExport extends SegmentNode {
  /**
   * The destination location of the segment.
   * @type string
   */
  destination: string;

  /**
   * Node type
   * @type
   */

  /**
   * The origin location of the segment.
   * @type string
   */
  origin: string;
}
