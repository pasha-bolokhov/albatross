import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';

import { ActivityExport } from '../exports/activity-export';
import { assertNever } from '../exports/assertNever';
import { Flight } from '../models/flight';
import { Hotel } from '../models/hotel';
import { Package } from '../models/package';
import { PackageExport } from '../exports/package-export';
import { PackageViewExport } from '../exports/package-view-export';
import { FlightExport } from '../exports/flight-export';
import { HotelExport, HotelInformationExport } from '../exports/hotel-export';
import { RentalExport } from '../exports/rental-export';
import { Segment } from '../models/segment';
import { SegmentNode, NodeType } from '../exports/segment-node';
import { TransportExport } from '../exports/transport-export';
import { TransportType } from '../models/transport';

/**
 * Processes an individual package
 *
 * @param database      FirebaseFirestore.Firestore         the instance of the database
 * @param packageId     string                              the id of the package document
 * @param packageData   FirebaseFirestore.DocumentData      the contents of the package document
 */
export async function processPackage(database: FirebaseFirestore.Firestore, packageId: string, packageData: FirebaseFirestore.DocumentData): Promise<any> {
  const data = <Package> packageData;
  const packageExport = <PackageExport> { ...data, id: packageId };
  const packageViewExport = <PackageViewExport> { ...data, id: packageId };
  const segments = await database.collection(`packages/${packageId}/segments`).orderBy('position', 'asc').get();
  packageViewExport.segments = [];
  await Promise.all(segments.docs.map((doc, index) => processSegment(<Segment> doc.data(), index, packageViewExport, packageId)));

  const newPackageExportDocRef = database.doc(`packageExports/${packageId}`);
  const newPackageViewExportDocRef = database.doc(`packageViewExports/${packageId}`);
  return Promise.all([
    newPackageExportDocRef.set(packageExport),
    newPackageViewExportDocRef.set(packageViewExport)
  ]);
}

/**
 * Walk through a segment, extract all data from it and place it
 * into the corresponding structures in 'exportObject'
 *
 * @param segment           the segment to process
 * @param segmentIndex      the position of the segment in the array of segments
 * @param exportObject      the object where to store the resulting values
 * @param packageId         the ID of the package
 */
async function processSegment(segment: Segment, segmentIndex: number, exportObject: PackageViewExport, packageId: string):
  Promise<FirebaseFirestore.DocumentSnapshot<any> | void> {
  let segmentNode: SegmentNode = { nodeType: undefined };
  switch (segment.type) {
  case 'transport':
    switch (segment.transportType) {
    case 'bike':
    case 'boat':
    case 'bus':
    case 'ferry':
    case 'train':
      const transportNodeTypes: Record<Exclude<TransportType, "flight">, NodeType> = {
        "bike": "bike",
        "boat": "boat",
        "bus": "bus",
        "ferry": "ferry",
        "train": "train"
      };
      const transportNode: TransportExport = {
        origin: segment.origin ? segment.origin : "",
        destination: segment.destination ? segment.destination : "",
        nodeType: transportNodeTypes[segment.transportType]
      };
      segmentNode = transportNode;
      break;

    case 'flight':
      const flightRef = segment.flight;
      if (!flightRef) {
        console.log(`processSegment(): flight not found`);
        break;
      }
      const flightDoc: DocumentSnapshot = await flightRef.get();
      const flightData = <Flight> flightDoc.data();
      if (!flightData) {
        console.log(`processSegment(): flight ${JSON.stringify(flightRef)} not found`);
        break;
      }
      const flightNode: FlightExport = { ...segmentNode, nodeType: "flight+", ...flightData };   // merge flight data and segment data
      segmentNode = flightNode;
      const flightExport: FlightExport = { ...flightData, nodeType: "flightInformation" };   // also send flight data to the nested node
      segmentNode.children = [flightExport];
      break;

    case undefined:
      console.log(`processSegment(): transportType ${segment.transportType}`);
      return;

    default:
      assertNever(segment.transportType);
    }
    break;

  case 'activity':
    const activityExport: ActivityExport = {
      activity: segment.activity ? segment.activity : undefined,
      location: segment.location ? segment.location : undefined,
      nodeType: "activity+",
    };
    const activity = segment.activity;
    switch (activity) {
    case 'Cruise':
    case 'Fishing':
    case 'Sightseeing':
    case 'Surfing':
      const children: SegmentNode[] = [];
      if (segment.hotel) {
        const hotelRef = segment.hotel;
        const hotelDoc: DocumentSnapshot = await hotelRef.get();
        const hotelData = <Hotel> hotelDoc.data();
        if (!hotelData) {
          console.log(`processSegment(): hotel ${JSON.stringify(hotelRef)} not found`);
          break;
        }
        const hotelExport: HotelExport = { nodeType: "hotel+", name: hotelDoc.id, duration: segment.duration };
        hotelExport.children = [<HotelInformationExport> { ...hotelData, name: hotelDoc.id, nodeType: "hotelInformation" }];
        children.push(hotelExport);
      }
      if (segment.rental) {
        const rentalExport: RentalExport = { nodeType: "rental", rentalType: segment.rental };
        children.push(rentalExport);
      }

      if (children.length > 0) {
        activityExport.children = children;
      }
    }
    segmentNode = activityExport;
    break;

    default:
      assertNever(segment.type);
  }
  exportObject.segments[segmentIndex] = segmentNode;
  return;
}
