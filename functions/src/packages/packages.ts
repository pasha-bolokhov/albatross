/**
 * @fileoverview Functions for processing package information.
 */
import * as functions from 'firebase-functions';

import { db } from '../init';
import { processPackage } from './process/package-document';

/**
 * The name of the package that is in fact a trigger
 * for processing all of the packages
 */
const UPDATE_TRIGGER_PACKAGE_ID: string = 'updateTrigger';

/**
 * Processes an update to any document in 'packages' collection.
 * If the id of the document is UPDATE_TRIGGER_PACKAGE_ID — process all documents in the collection.
 *
 * An updated package information is collected and stored into the corresponding
 * document in 'packageExports' collection
 *
 * @param change            The change that triggered this function
 * @param context           The context of the change
 */
export const onPackageChange = functions.firestore.document('/packages/{id}').onWrite(async (change, context): Promise<any> => {
  const id: string | undefined = context.params.id;
  if (!id) {
    return null;
  }

  // Update all packages
  if (id === UPDATE_TRIGGER_PACKAGE_ID) {
    console.log(`updating all packages`);
    const allPackages = await db.collection(`packages`).get();
    return allPackages.docs.filter(doc => doc.id !== UPDATE_TRIGGER_PACKAGE_ID).map(doc => processPackage(db, doc.id, doc.data()));
  }

  // Check if the change was a deletion
  if (!change.after.exists) {
    console.log(`package "${id}" has been deleted`);
    return Promise.all([db.doc(`packageExports/${id}`).delete(), db.doc(`packageViewExports/${id}`).delete()]);
  }

  // We ignore any data that already might be in 'packageExports' or 'packageViewExports'
  const changedData = change.after.data();
  if (changedData) {
    return processPackage(db, id, changedData);
  }
  return null;
});
