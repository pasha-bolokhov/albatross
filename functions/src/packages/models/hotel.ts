/**
 * Hotel information as stored in the Firestore
 */
export interface Hotel {
  /**
   * The verbose description of the hotel.
   * @type string
   */
  description: string;

  /**
   * The ranking number (stars) of the hotel.
   * @type number
   */
  rank: number;
}
