import { Flight } from './flight';
import { Hotel } from './hotel';
import { Activity } from './activity';
import { Rental } from './rental';
import { TransportType } from './transport';

/**
 * The type for classification of segment
 */
export type SegmentType = "activity" | "transport";

/**
 * Segment information as stored in the Firestore
 */
export interface Segment {
  /**
   * Main activity of the segment.
   * Present when 'type' equals "activity".
   * Optional.
   * @type Activity
   */
  activity?: Activity;

  /**
   * The destination of the segment.
   * Present when 'type' equals "transport"
   * Optional.
   * @type string
   */
  destination?: string;

  /**
   * The duration of the segment in days.
   * @type number
   */
  duration: number;

  /**
   * A reference to the flight document in the Firestore.
   * Present when 'type' equals "transport" and 'transportType' equals "flight".
   * Optional.
   * @type FirebaseFirestore.DocumentReference<Flight>
   */
  flight?: FirebaseFirestore.DocumentReference<Flight>;

  /**
   * A reference to the hotel document in the Firestore.
   * Optional.
   * @type FirebaseFirestore.DocumentReference<Hotel>
   */
  hotel?: FirebaseFirestore.DocumentReference<Hotel>;

  /**
   * The main location of the segment.
   * Present when 'type' equals "activity".
   * Optional.
   * @type string
   */
  location?: string;

  /**
   * The origin location of the segment.
   * Present when 'type' equals "transport".
   * Optional.
   * @type string
   */
  origin?: string;

  /**
   * The ordinal position of the segment.
   * @type number
   */
  position: number;

  /**
   * The type of rental vehicle of the segment.
   * Optional.
   * @type Rental
   */
  rental?: Rental;

  /**
   * The type of transport the segment represents.
   * Present when 'type' equals "transport".
   * Optional.
   * @type TransportType
   */
  transportType?: TransportType;

  /**
   * The main type of the segment which determines how the other properties are interpreted and used.
   * @type SegmentType
   */
  type: SegmentType;
}
