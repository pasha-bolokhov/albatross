/**
 * The type for a rental
 */
export type Rental = "compact" | "economy" | "full size" | "luxury" | "minivan" | "standard" | "SUV" | "van";
