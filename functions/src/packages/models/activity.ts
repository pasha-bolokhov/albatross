/**
 * The type for an activity
 */
export type Activity = "Cruise" | "Fishing" | "Sightseeing" | "Surfing";
