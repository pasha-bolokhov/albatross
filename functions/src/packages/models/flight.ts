/**
 * Flight information as stored in the Firestore
 */
export interface Flight {
  /**
   * The origin location of the flight.
   * @type string
   */
  origin: string;

  /**
   * The destination location of the flight.
   * @type string
   */
  destination: string;

  /**
   * Flight number.
   * @type string
   */
  flightNo: string;

  /**
   * The departure date of the flight.
   * @type Date
   */
  departureDate: Date;

  /**
   * The arrival date of the flight.
   * @type Date
   */
  arrivalDate: Date;
}
