/**
 * The model of a package as stored in the Firestore
 */
export interface Package {
  /**
   * The number of packages available.
   * @type number
   */
  available: number;

  /**
   * The verbose description of the package.
   * @type string
   */
  description: string;

  /**
   * The main destination of the package.
   * @type string
   */
  destination: string;

  /**
   * The URL of the image representing the package.
   * @type string
   */
  imageUrl: string;

  /**
   * The full name of the package.
   * @type string
   */
  name: string;

  /**
   * The region referring to 'location'.
   * @type string
   */
  region: string;

  /**
   * The main origin of the package.
   * @type string
   */
  origin: string;

  /**
   * The ordinal position of the package for display purposes.
   * @type number
   */
  position: number;

  /**
   * The price of the package.
   * @type number
   */
  price: number;
}

/**
 * A type guard for interface Package
 *
 * @param   {any}       candidate               The object to check for compliance with Package
 *
 * @returns {boolean}                           Whether 'candidate' is an instance of Package
 */
export function isPackage(candidate: any): candidate is Package {
  if (candidate === null || candidate === undefined) {
    return false;
  }

  const keys: Record<keyof Package, true> = {
    available: true,
    description: true,
    destination: true,
    imageUrl: true,
    name: true,
    region: true,
    origin: true,
    position: true,
    price: true,
  };

  for (const key in keys) {
    if (candidate[key] === undefined) {
      return false;
    }
  }

  return true;
}
