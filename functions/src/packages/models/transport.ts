/**
 * The type for a transport
 */
export type TransportType = "bike" | "boat" | "bus" | "ferry" | "flight" | "train";
