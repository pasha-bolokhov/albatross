/**
 * @file Global definitions
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */

/**
 * Utility type to extract required properties from an interface
 */
export type RequiredKeys<T> = { [K in keyof T]-?: {} extends Pick<T, K> ? never : K }[keyof T];
