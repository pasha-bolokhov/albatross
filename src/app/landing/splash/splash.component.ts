import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { SplashDialogueComponent } from './splash-dialogue.component';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {

  constructor(private dialogue: MatDialog, private router: Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      const dialogueRef = this.dialogue.open(SplashDialogueComponent, {
        data: {},
        panelClass: 'splash-dialogue',
      });
      dialogueRef.afterClosed().subscribe(_ => {
        this.router.navigate(["landing"]);
      });
    }, 2000);
  }
}
