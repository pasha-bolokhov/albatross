import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { ServiceWorkerModule } from '@angular/service-worker';

import { AppComponent } from './app.component';
import { AuthButtonsComponent } from './auth-buttons/auth-buttons.component';
import { EmailSignInDialogueComponent } from './auth/sign-in/email-sign-in-dialogue.component';
import { LandingComponent } from './landing/landing.component';
import { LogOutComponent } from './auth/log-out/log-out.component';
import { LogOutDialogueComponent } from './auth/log-out/log-out-dialogue.component';
import { SecureModeComponent } from './secure-mode/secure-mode.component';
import { SignInMethodDialogueComponent } from './auth/sign-in/sign-in-method-dialogue.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { SplashComponent } from './landing/splash/splash.component';
import { SplashDialogueComponent } from './landing/splash/splash-dialogue.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthButtonsComponent,
    EmailSignInDialogueComponent,
    LandingComponent,
    LogOutComponent,
    LogOutDialogueComponent,
    SecureModeComponent,
    SignInMethodDialogueComponent,
    SignUpComponent,
    SplashComponent,
    SplashDialogueComponent,
  ],
  entryComponents: [
    EmailSignInDialogueComponent,
    SignInMethodDialogueComponent,
    SplashDialogueComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    // AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
