import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthButtonsComponent } from './auth-buttons/auth-buttons.component';
import { AuthGuard } from './auth/auth.guard';
import { SecureModeComponent } from './secure-mode/secure-mode.component';
import { LandingComponent } from './landing/landing.component';
import { LogOutComponent } from './auth/log-out/log-out.component';
import { SplashComponent } from './landing/splash/splash.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing/splash',
    pathMatch: 'full',
  },
  {
    path: 'landing',
    component: LandingComponent,
    children: [{
      path: 'splash',
      component: SplashComponent,
    }],
  },
  {
    path: 'cart',
    loadChildren: () => import('./cart/cart.module').then(m => m.CartModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'packages',
    loadChildren: () => import('./packages/packages.module').then(m => m.PackagesModule),
  },
  {
    path: 'logout',
    component: LogOutComponent,
  },
  {
    path: '',
    component: AuthButtonsComponent,
    outlet: 'secure',
  },
  {
    path: 'user',
    component: SecureModeComponent,
    outlet: 'secure',
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
