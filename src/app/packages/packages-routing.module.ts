import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PackagesComponent } from './packages.component';
import { PreviewComponent } from './preview/preview.component';

const routes: Routes = [
  {
    path: '', component: PackagesComponent
  },
  {
    path: 'view/:id', component: PreviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PackagesRoutingModule { }
