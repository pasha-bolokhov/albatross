import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AdventureComponent } from './adventure/adventure.component';
import { PackagesRoutingModule } from './packages-routing.module';
import { PackagesComponent } from './packages.component';
import { PreviewComponent } from './preview/preview.component';

import { MaterialModule } from '../material.module';


@NgModule({
  declarations: [PackagesComponent, AdventureComponent, PreviewComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    PackagesRoutingModule,
    MaterialModule,
  ]
})
export class PackagesModule { }
