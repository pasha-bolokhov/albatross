import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { Trip } from './adventure/trip';
import { TripView } from './adventure/trip-view';


@Injectable({
  providedIn: 'root'
})
export class AdventureService {
  constructor(private firestore: AngularFirestore) { }

  getPackages(): Observable<Trip[]> {
    return this.firestore.collection<Trip>('packageExports').valueChanges();
  }

  getPackageView(id: string): Observable<TripView> {
    return this.firestore.doc<TripView>(`packageViewExports/${id}`).valueChanges();
  }
}
