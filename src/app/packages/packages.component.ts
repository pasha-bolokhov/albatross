import { Component, OnInit } from '@angular/core';
import { AdventureService } from './adventure.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { Trip } from './adventure/trip';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {
  trips: Observable<Trip[]>;

  constructor(private adventureService: AdventureService) { }

  ngOnInit() {
    this.getPackages();
  }

  getPackages(): void {
    this.trips = this.adventureService.getPackages().pipe(
      map(
        packages => packages.sort((a, b) => a.position - b.position)
      )
    );
  }
}
