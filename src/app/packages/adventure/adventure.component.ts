import { Component, OnInit, Input } from '@angular/core';
import { Trip } from './trip';

@Component({
  selector: 'app-adventure',
  templateUrl: './adventure.component.html',
  styleUrls: ['./adventure.component.scss']
})
export class AdventureComponent implements OnInit {
  @Input() adventure: Trip;

  constructor() { }

  ngOnInit(): void {
  }
}
