/**
 * @file The preview of a package before the user sends it to the cart
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { ActivatedRoute, ParamMap } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  faAnchor, faBicycle, faBinoculars, faBus, faCameraRetro, faCar, faChevronDown, faChevronRight,
  faFish, faHotel, faPlane, faShip, faTrain, faWater, IconDefinition
} from '@fortawesome/free-solid-svg-icons';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

import { catchError, map, mergeMap, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { defer, iif, Observable, of, Subject, throwError, zip } from 'rxjs';

import { AdventureService } from '../adventure.service';
import { ActivityExport, ActivityType } from 'functions/src/packages/exports/activity-export';
import { AuthService } from 'src/app/auth/auth.service';
import { assertNever } from 'functions/src/packages/exports/assertNever';
import { CartSubmission, CartSubmissionErrorType, isCartSubmission, SubmissionError } from 'functions/src/cart/models/cart-submission';
import { HotelInformationExport } from 'functions/src/packages/exports/hotel-export';
import { LoginService } from 'src/app/auth/login.service';
import { NavService } from 'src/app/nav/nav.service';
import { NodeType } from 'functions/src/packages/exports/segment-node';
import { SegmentNode, TripView } from '../adventure/trip-view';

import { MatSnackBar } from '@angular/material/snack-bar';    // AAAA this is probably temporary

/** Flat node with expandable and level information */
interface FlatNode {
  /**
   * Whether this node can be expanded.
   * Needed for MatTree.
   * @type boolean
   */
  expandable: boolean;

  /**
   * Icon to display as the tree node.
   * Optional.
   * @type IconDefinition
   */
  icon?: IconDefinition;

  /**
   * The level of nestedness of this node.
   * Needed for MatTree.
   * @type number
   */
  level: number;

  /**
   * The type of the node.
   * See "SegmentNode"
   */
  nodeType: NodeType;

  /**
   * A string that symbolizes the rank of hotel.
   * Optional.
   * @type string
   */
  displayRank?: string;
}

/**
 * @title Package preview component
 */
@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition('void => open', [
        style({
          opacity: 0,
          transform: "translateX(-2rem)",
        }),
        animate('400ms', style({
            opacity: 1,
            transform: "none",
        })),
      ]),
      transition('open => void', [
        animate('400ms', style({
            opacity: 0,
            transform: "translateX(-2rem)",
        })),
      ]),
    ]) // trigger
  ] // animations
})
export class PreviewComponent implements OnInit, OnDestroy {
  private ngOnDestroy$: Subject<any> = new Subject();

  /**
   * The trip information to show
   */
  adventureView$: Observable<TripView>;

  /**
   * FontAwesome icons
   */
  faChevronRight = faChevronRight;
  faChevronDown = faChevronDown;

  /**
   * Whether 'Go' button is disabled
   */
  goButtonDisabled = false;

  private _transformer = (node: SegmentNode, level: number): FlatNode => {
    const { children: _, ...nodeInfo } = node;   // Get rid of "children"
    const flatNode: FlatNode = {
      ...nodeInfo,
      expandable: !!node.children && node.children.length > 0,
      icon: null,         // defined below
      level: level,
    };
    switch (node.nodeType) {
      case "activity+":
        const activityNode: ActivityExport = node as ActivityExport;
        const activityIconTable: Record<ActivityType, IconDefinition> = {
          "Cruise": faBinoculars,
          "Fishing": faFish,
          "Sightseeing": faCameraRetro,
          "Surfing": faWater,
        };
        flatNode.icon = activityIconTable[activityNode.activity];
        break;

      case "flight+":
        flatNode.icon = faPlane;
        break;

      case "flightInformation":
        break;

      case "hotel+":
        flatNode.icon = faHotel;
        break;

      case 'hotelInformation':
        flatNode.displayRank = '⭐'.repeat((<HotelInformationExport> node).rank);
        break;

      case "bike":
        flatNode.icon = faBicycle;
        break;

      case "boat":
        flatNode.icon = faShip;
        break;

      case "bus":
        flatNode.icon = faBus;
        break;

      case "ferry":
        flatNode.icon = faAnchor;

      case "train":
        flatNode.icon = faTrain;
        break;

      case "rental":
        flatNode.icon = faCar;
        break;

      default:
        assertNever(node.nodeType);
    }
    return flatNode;
  }

  treeControl = new FlatTreeControl<FlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  hasChild = (_: number, node: FlatNode) => node.expandable;

  constructor(private route: ActivatedRoute, private firestore: AngularFirestore, private adventureService: AdventureService, private authService: AuthService,
              private loginService: LoginService, private navService: NavService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.adventureView$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.adventureService.getPackageView(params.get('id')))
    );
    this.adventureView$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(packageExport => this.dataSource.data = packageExport.segments);
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
    this.ngOnDestroy$.complete();
  }

  /**
   * Error messages to display upon a submission attempt
   */
  private errorMessages: Record<CartSubmissionErrorType, string> = {
    "application-error": "Application Error. Reload the page and try again",
    "internal-error": "Error occured. Try again later",
    "package-not-available": "Package is not available anymore. Come back later",
  };

  /**
   * Submits the package to the cart and, when notified, navigates to the cart page
   */
  go(): void {
    /**
     * Try to eliminate the effect of multiple clicks on 'Go' button
     */
    if (this.goButtonDisabled) {
      return;
    }
    this.goButtonDisabled = true;

    // Determines when to stop listening to the Firestore
    const finish$ = new Subject<boolean>();

    this.adventureView$.pipe(
      takeUntil(this.ngOnDestroy$),
      take(1),
      takeUntil(finish$),

      // Check availability of the package
      map((view: TripView): [string, number] => [view.id, view.available]),
      tap(([_, available]) => {
        if (available < 1) {
          throw new SubmissionError("package-not-available");
        }
      }),

      // Ensure the user is authenticated
      switchMap(([packageId, _]) => zip(
        of(packageId),
        defer(() => this.navService.navigateToUserMode())
      )),
      mergeMap(([packageId, authenticated]) => iif(
        () => authenticated,
        // Add the package to the cart in the form of CartSubmission
        this.authService.signedUser().pipe(
          map((user: firebase.User): string => user.uid),
          take(1),
          map((uid): [string, CartSubmission] => ([uid, {packageId}])),
          switchMap(([uid, submission]): Promise<DocumentReference> => this.firestore.collection(`/users/${uid}/cartSubmissions`).add(submission)),
          switchMap((docRef: DocumentReference) => this.firestore.doc(docRef.path).valueChanges()),
          takeUntil(finish$),

          // Handle errors in response
          map((updatedSubmission: any): boolean => {
            if (!isCartSubmission(updatedSubmission)) {
              throw new SubmissionError("internal-error");
            }
            if (updatedSubmission.status !== undefined && updatedSubmission.status !== "success") {
              throw new SubmissionError(updatedSubmission.status);
            }

            return updatedSubmission.status === "success";
          }),

          // Switch to the Cart page
          switchMap((successful: boolean) => iif(
            () => successful,
            defer(() => {
              finish$.next(true);                           // Indicate that we do not listen to the Firestore anymore
              return this.navService.navigateToCart();
            })
          )),
        )
      )),
      catchError((error) => throwError((error instanceof SubmissionError) ? error : new SubmissionError("internal-error"))),
      catchError((error) => {
        const message = this.errorMessages[error.message];
        // AAAA
        this.snackBar.open(message, `Dismiss`, {duration: 4000});
        return throwError(error);
      })
    ).subscribe();
  }
}
