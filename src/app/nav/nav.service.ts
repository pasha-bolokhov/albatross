import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export const NAV_USER_MODE = {outlets: {secure: "user"}};
export const NAV_EMPTY_MODE = {outlets: {secure: null}};

/**
 * This service performs common navigation tasks.
 * It forms an abstract layer on top of the Router
 */
@Injectable({
  providedIn: 'root'
})
export class NavService {
  constructor(public router: Router) { }

  public async navigateToUserMode(): Promise<boolean> {
    const active = this.router.isActive('(secure:user)', false);
    if (active) {
      return true;
    }
    return await this.router.navigate([NAV_USER_MODE]);
  }

  public navigateToEmptyMode(): Promise<boolean> {
    return this.router.navigate([NAV_EMPTY_MODE]);
  }

  public async clearAndNavigateToLanding(): Promise<boolean> {
    await this.router.navigate([NAV_EMPTY_MODE]);
    return this.router.navigate(["landing"]);
  }

  public navigateToCart(): Promise<boolean> {
    return this.router.navigate(["cart"]);
  }
}
