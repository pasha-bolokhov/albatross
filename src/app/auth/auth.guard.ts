import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { AuthService } from './auth.service';
import { LoginService } from './login.service';
import { NAV_EMPTY_MODE } from '../nav/nav.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private loginService: LoginService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.signedUser().pipe(
      switchMap(user =>
        user !== null ?
          of(true) :
          this.loginService.openSignInDialogue().pipe(
            map(newUser => newUser !== null ? true : this.router.createUrlTree([NAV_EMPTY_MODE]))
          )
      )
    );
  }
}
