import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogOutDialogueComponent } from './log-out-dialogue.component';

describe('LogOutDialogueComponent', () => {
  let component: LogOutDialogueComponent;
  let fixture: ComponentFixture<LogOutDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogOutDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogOutDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
