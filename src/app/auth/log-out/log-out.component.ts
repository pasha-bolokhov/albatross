import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { NavService } from '../../nav/nav.service';

/**
 * This component behaves like an API call, in that it does not ask for confirmation or wait for any completion
 */
@Component({
  selector: 'app-log-out',
  templateUrl: './log-out.component.html',
  styleUrls: ['./log-out.component.scss']
})
export class LogOutComponent implements OnInit {
  constructor(private authService: AuthService, private navService: NavService) { }

  ngOnInit(): void {
    this.authService.signOut();
    this.navService.clearAndNavigateToLanding();
  }
}
