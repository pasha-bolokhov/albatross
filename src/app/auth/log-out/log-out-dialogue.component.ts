import { Component } from '@angular/core';

@Component({
  selector: 'app-logout-dialogue',
  templateUrl: './log-out-dialogue.component.html',
  styleUrls: ['./log-out-dialogue.component.scss']
})
export class LogOutDialogueComponent {
  constructor() { }
}
