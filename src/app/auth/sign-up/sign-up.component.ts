import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up-component',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  info = new FormGroup({
    name: new FormControl('', [
      Validators.required,
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    pass: new FormControl('', [
      Validators.required,
      Validators.pattern(/[A-Z]/),
      Validators.pattern(/[0-9]/),
    ]),
    passRetyped: new FormControl('', [
      Validators.required,
      RxwebValidators.compare({ fieldName: 'pass' }),
    ])
  });

  constructor (public dialogue: MatDialogRef<SignUpComponent>, private authService: AuthService) {}

  async signUp(): Promise<void> {
    try {
      const result: firebase.auth.UserCredential = await this.authService.signUpWithEmailAndPassword(this.info.value.name, this.info.value.email, this.info.value.pass);
      this.dialogue.close(result);
    } catch (error) {
      if (!error?.code) {
        return;
      }
      switch (error.code) {
        case 'auth/invalid-email':
          this.info.controls.email.setErrors({'invalid': true});
          break;
        case 'auth/email-already-in-use':
          this.info.controls.email.setErrors({'inUse': true});
          break;
        case 'auth/weak-password':
          this.info.controls.pass.setErrors({'weak': true});
          break;
        default:
          this.info.setErrors({'unknown': true});
      }
      // AAAA Deal with the error
      console.log(`%c Error signing up `, `color: white; background: fuchsia`);
      console.log(error);
    }
  }
}
