import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInMethodDialogueComponent } from './sign-in-method-dialogue.component';

describe('SignInMethodDialogueComponent', () => {
  let component: SignInMethodDialogueComponent;
  let fixture: ComponentFixture<SignInMethodDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInMethodDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInMethodDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
