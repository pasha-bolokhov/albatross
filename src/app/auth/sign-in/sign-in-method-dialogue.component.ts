import { Component } from '@angular/core';

const signInMethodValues = [
  "signInAnonymously",
  "signInWithEmail",
  "signInWithGoogle",
  "signInWithFacebook",
  "signInWithMicrosoft",
  "signInWithGithub",
  "signInMethodCancelled"
] as const;
export type SignInMethod = typeof signInMethodValues[number];

/**
 * A type guard for SignInMethod
 *
 * @param   {string}            method                      The method value to check the type of
 * @returns {boolean}                                       Whether 'method' is of the type 'SignInMethod'
 */
export function isSignInMethod(method: string): method is SignInMethod {
  return (signInMethodValues as readonly string[]).includes(method);
}

@Component({
  selector: 'app-sign-in-method-dialogue',
  templateUrl: './sign-in-method-dialogue.component.html',
  styleUrls: ['./sign-in-method-dialogue.component.scss']
})
export class SignInMethodDialogueComponent {
  constructor() {}
}
