import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-email-sign-in',
  templateUrl: './email-sign-in-dialogue.component.html',
  styleUrls: ['./email-sign-in-dialogue.component.scss']
})
export class EmailSignInDialogueComponent {
  info = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    pass: new FormControl('', [
      Validators.required,
    ]),
  });

  constructor(public dialogue: MatDialogRef<EmailSignInDialogueComponent>, private authService: AuthService) { }

  async signIn(): Promise<void> {
    try {
      const result = await this.authService.signInWithEmailAndPassword(this.info.value.email, this.info.value.pass);
      if (result?.user?.uid) {
        this.dialogue.close(result);
        return;
      }
      this.info.setErrors({'unknown': true})
    } catch (error) {
      if (!error?.code) {
        return;
      }
      switch (error.code) {
        case 'auth/user-not-found':
          this.info.controls.email.setErrors({'unknown': true});
          break;
        case 'auth/wrong-password':
          this.info.controls.pass.setErrors({'wrong': true});
          break;
        default:
          this.info.setErrors({'unknown': true});
      }
    }
  }
}
