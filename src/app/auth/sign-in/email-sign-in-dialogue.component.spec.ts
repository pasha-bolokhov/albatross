import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSignInDialogueComponent } from './email-sign-in-dialogue.component';

describe('EmailSignInDialogueComponent', () => {
  let component: EmailSignInDialogueComponent;
  let fixture: ComponentFixture<EmailSignInDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailSignInDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSignInDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
