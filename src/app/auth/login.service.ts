import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OnDestroy } from '@angular/core';
import { from, Observable, of, Subject, throwError } from 'rxjs';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { EmailSignInDialogueComponent } from './sign-in/email-sign-in-dialogue.component';
import { LogOutDialogueComponent } from './log-out/log-out-dialogue.component';
import { NavService } from '../nav/nav.service';
import { isSignInMethod, SignInMethod, SignInMethodDialogueComponent } from './sign-in/sign-in-method-dialogue.component';
import { SignUpComponent } from './sign-up/sign-up.component';


@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {
  private ngOnDestroy$: Subject<any> = new Subject();

  private signInAnonymously: () => Observable<firebase.User> = () => {
    return from(this.authService.signInAnonymously()).pipe(
      switchMap(signInAnonymouslyResult => {
        if (signInAnonymouslyResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Created anonymous account`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInAnonymouslyResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Anonymous sign in error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInWithEmail: () => Observable<firebase.User> = () => {
    const signInDialogue = this.dialogue.open(EmailSignInDialogueComponent, {
      autoFocus: false,
      data: {},
      panelClass: 'albatross-dialogue'
    });
    return signInDialogue.afterClosed().pipe(
      switchMap(signInWithEmailResult => {
        if (signInWithEmailResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Signed in successfully`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInWithEmailResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Sign in with email error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInWithGoogle: () => Observable<firebase.User> = () => {
    return from(this.authService.signInWithGoogle()).pipe(
      switchMap(signInWithGoogleResult => {
        if (signInWithGoogleResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Signed in with your Google account`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInWithGoogleResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Google sign in error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInWithFacebook: () => Observable<firebase.User> = () => {
    return from(this.authService.signInWithFacebook()).pipe(
      switchMap(signInWithFacebookResult => {
        if (signInWithFacebookResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Signed in with your Facebook account`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInWithFacebookResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Facebook sign in error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInWithMicrosoft: () => Observable<firebase.User> = () => {
    return from(this.authService.signInWithMicrosoft()).pipe(
      switchMap(signInWithMicrosoftResult => {
        if (signInWithMicrosoftResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Signed in with your Microsoft account`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInWithMicrosoftResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Microsoft sign in error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInWithGithub: () => Observable<firebase.User> = () => {
    return from(this.authService.signInWithGithub()).pipe(
      switchMap(signInWithGithubResult => {
        if (signInWithGithubResult?.user?.uid) {      // Switch to the user mode if the user is valid
          this.snackBar.open(`Signed in with your Github account`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToUserMode())
            .pipe(map(() => signInWithGithubResult.user));
        }
        return of(null);
      }),
      catchError(error => {
        // AAAA
        this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
        console.log(`%c Not implemented yet `, `color: white; background: violet`);
        console.log(`Microsoft sign in error:`);
        console.log(error);
        return throwError(error);
      })
    );
  };

  private signInMethodCancelled: () => Observable<firebase.User> = () => {
    return this.authService.signedUser();
  };

  private signInMethodMap: Record<SignInMethod, () => Observable<firebase.User>> = {
    "signInAnonymously": this.signInAnonymously,
    "signInWithEmail": this.signInWithEmail,
    "signInWithGoogle": this.signInWithGoogle,
    "signInWithFacebook": this.signInWithFacebook,
    "signInWithMicrosoft": this.signInWithMicrosoft,
    "signInWithGithub": this.signInWithGithub,
    "signInMethodCancelled": this.signInMethodCancelled,
  };

  constructor(public dialogue: MatDialog, private snackBar: MatSnackBar,
              private navService: NavService, private authService: AuthService) {}

  ngOnDestroy() {
    this.ngOnDestroy$.next();
    this.ngOnDestroy$.complete();
  }

  openSignInDialogue(): Observable<firebase.User> {
    const dialogueRef = this.dialogue.open(SignInMethodDialogueComponent, {
      autoFocus: false,
      data: {},
      panelClass: 'albatross-dialogue'
    });
    return dialogueRef.afterClosed().pipe(
      takeUntil(this.ngOnDestroy$),
      map(choice => choice !== undefined ? choice : "signInMethodCancelled"),
      map(choice => {
        if (isSignInMethod(choice)) {
          return choice;
        }
        throw new Error(`Invalid sign in method '${choice}'`);
      }),
      switchMap((choice: SignInMethod) => this.signInMethodMap[choice]())
    );
  }

  openSignUpDialogue(): void {
    const dialogueRef = this.dialogue.open(SignUpComponent, {
      autoFocus: false,
      data: {},
      panelClass: 'albatross-dialogue'
    });
    dialogueRef.afterClosed().pipe(takeUntil(this.ngOnDestroy$), switchMap(signUpResult => {
      if (signUpResult?.user?.uid) {      // Switch to the user mode if the user is valid
        this.snackBar.open(`Account created successfully`, `Dismiss`, {duration: 2000});
        return from(this.navService.navigateToUserMode());
      }
      return of(false);
    })).subscribe();
  }

  openLogOutDialogue(): void {
    const logOutDialogue = this.dialogue.open(LogOutDialogueComponent, {
      autoFocus: false,
      data: {},
      panelClass: 'albatross-dialogue'
    });
    logOutDialogue.afterClosed().pipe(takeUntil(this.ngOnDestroy$), switchMap(logOutResult => {
      if (logOutResult !== "log-out") {
        return of(false);
      }
      return from(this.authService.signOut()).pipe(
        switchMap(() => {
          this.snackBar.open(`Signed out successfully`, `Dismiss`, {duration: 2000});
          return from(this.navService.navigateToEmptyMode());       // Do not wait for the completion of signOut()
        }),
        catchError(error => {
          this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
          return throwError(error);
        })
    )})).subscribe();
  }

  async openDeleteAccountDialogue(): Promise<void | boolean> {
    // AAAA this should really open a confirmation dialogue
    try {
      await this.authService.deleteUser();
      this.snackBar.open(`Your account has been closed`, `Dismiss`, {duration: 2000});
      return this.navService.navigateToEmptyMode();
    } catch (error) {
      // AAAA process the case when the user needs to re-authenticate in order to delete their account
      this.snackBar.open(`An error occurred`, `Dismiss`, {duration: 2000});
    }
  }
}
