import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { auth } from 'firebase/app';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private afAuth: AngularFireAuth, private firestore: AngularFirestore) { }

  async signUpWithEmailAndPassword(name: string, email: string, password: string): Promise<firebase.auth.UserCredential> {
      const userCredential = await this.afAuth.createUserWithEmailAndPassword(email, password);
      await userCredential.user.updateProfile({displayName: name});
      if (userCredential.user?.uid) {
        await this.firestore.doc(`/users/${userCredential.user.uid}`).set({name: name, mode: 'temporary'}, {merge: true});
      }
      return userCredential;
  }

  signInWithEmailAndPassword(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  async signInAnonymously(): Promise<firebase.auth.UserCredential> {
    const userCredential = await this.afAuth.signInAnonymously();
    await userCredential.user.updateProfile({displayName: 'Anonymous'});
    if (userCredential.user?.uid) {
      await this.firestore.doc(`/users/${userCredential.user.uid}`).set({name: 'Anonymous', mode: 'temporary'}, {merge: true});
    }
    return userCredential;
  }

  async signInWithGoogle(): Promise<firebase.auth.UserCredential> {
    const provider = new auth.GoogleAuthProvider();
    return this.afAuth.signInWithPopup(provider);
  }

  async signInWithFacebook(): Promise<firebase.auth.UserCredential> {
    const provider = new auth.FacebookAuthProvider();
    provider.setCustomParameters({
      'display': 'popup'
    });
    return this.afAuth.signInWithPopup(provider);
  }

  async signInWithMicrosoft(): Promise<firebase.auth.UserCredential> {
    const provider = new auth.OAuthProvider('microsoft.com');
    return this.afAuth.signInWithPopup(provider);
  }

  async signInWithGithub(): Promise<firebase.auth.UserCredential> {
    const provider = new auth.GithubAuthProvider();
    provider.addScope('read:user');
    return this.afAuth.signInWithPopup(provider);
  }

  signedUser(): Observable<firebase.User> {
    return this.afAuth.user;
  }

  signOut(): Promise<void> {
    return this.afAuth.signOut();
  }

  async deleteUser(): Promise<void> {
    const currentUser = await this.afAuth.currentUser;
    return currentUser.delete();
  }
}
