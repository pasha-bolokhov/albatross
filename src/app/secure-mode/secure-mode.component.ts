import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';

import { map } from 'rxjs/operators';
import { combineLatest, Observable } from 'rxjs';

import { AuthService } from '../auth/auth.service';
import { LoginService } from '../auth/login.service';
import { NavService } from '../nav/nav.service';

@Component({
  selector: 'app-secure-mode',
  templateUrl: './secure-mode.component.html',
  styleUrls: ['./secure-mode.component.scss']
})
export class SecureModeComponent implements OnInit {
  currentUserName$: Observable<string>;
  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;
  smBreakpoint$: Observable<BreakpointState>;
  mdBreakpoint$: Observable<BreakpointState>;

  constructor(private breakpointObserver: BreakpointObserver, private snackBar: MatSnackBar,
              private loginService: LoginService, private authService: AuthService, private navService: NavService) {
    this.smBreakpoint$ = breakpointObserver.observe([
      '(min-width: 600px)'
    ]);
    this.mdBreakpoint$ = breakpointObserver.observe([
      '(min-width: 960px)'
    ]);
    this.smBreakpoint$.subscribe(() => {
        this.processMediaSizeChange();
    });
  }

  ngOnInit() {
    const username$ = this.authService.signedUser().pipe(
      map((user) => user?.displayName ? user.displayName : "user"),
      map((user) => this.truncateUsername(user, 20, 16))
    );
    this.currentUserName$ = combineLatest(this.mdBreakpoint$, username$).pipe(map(([_, name]) =>
      this.breakpointObserver.isMatched('(min-width: 960px)') ?
        name :
        this.truncateUsername(name, 10, 5)
    ));
  }

  private processMediaSizeChange(): void {
    // Close the menu when the screen is larger then 'xs'
    if (this.menuTrigger?.menuOpen && this.breakpointObserver.isMatched('(min-width: 600px)')) {
      this.menuTrigger.closeMenu();
    }
  }

  private truncateUsername(name:string, maxLength: number, truncateTo: number): string {
    return name.length <= maxLength ?
      name :
      (name.substr(0, truncateTo) + "...");
  }

  showCart(): void {
    this.navService.navigateToCart();
  }

  showProfile(): void {
    console.log(`%c Not implemented yet `, `color: white; background: violet`);       // AAAA
    this.snackBar.open(`Not implemented yet`, `Dismiss`, {duration: 2000, panelClass: 'not-implemented-yet-snack-bar'});
  }

  logOut(): void {
    this.loginService.openLogOutDialogue();
  }

  deleteAccount(): void {
    this.loginService.openDeleteAccountDialogue();
  }
}
