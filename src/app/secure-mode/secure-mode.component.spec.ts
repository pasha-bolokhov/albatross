import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureModeComponent } from './secure-mode.component';

describe('SecureModeComponent', () => {
  let component: SecureModeComponent;
  let fixture: ComponentFixture<SecureModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
