import { Component, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AuthService } from '../auth/auth.service';
import { CartItem } from './cart-item/cart-item';
import { CartService } from './cart.service';
import { NavService } from '../nav/nav.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartItems$: Observable<CartItem[]>;

  constructor(private authService: AuthService, private cartService: CartService, private navService: NavService) { }

  ngOnInit(): void {
    // AAAA check for errors
    this.navService.navigateToUserMode();
    this.getItems();
  }

  /**
   * Fetches cart items using CartService
   */
  private getItems(): void {
    this.cartItems$ = this.authService.signedUser().pipe(
      switchMap((user: firebase.User): Observable<CartItem[]> => this.cartService.getCartItems(user.uid))
    );
  }
}
