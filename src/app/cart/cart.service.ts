/**
 * @file The service which fetches and removes cart items from the Firestore
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { defer, from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CartExport, CartItem } from './cart-item/cart-item';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  constructor(private firestore: AngularFirestore) { }

  getCartItems(uid: string): Observable<CartItem[]> {
    return this.firestore.collection<CartExport>(`/users/${uid}/cart`).snapshotChanges().pipe(
      map(actions => actions.map(action => {
        const data = action.payload.doc.data();               // AAAA check validity of the data
        const id = action.payload.doc.id;
        return { id, uid, ...data } as CartItem;
      }))
    );
  }

  deleteCartItem(item: CartItem): Observable<void> {
    return defer(() => this.firestore.doc(`/users/${item.uid}/cart/${item.id}`).delete());
  }
}
