import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';
import { CartItemComponent } from './cart-item/cart-item.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material.module';


@NgModule({
  declarations: [CartComponent, CartItemComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    FlexLayoutModule,
    FontAwesomeModule,
    MaterialModule,
  ]
})
export class CartModule { }
