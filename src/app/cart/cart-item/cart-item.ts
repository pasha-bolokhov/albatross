/**
 * @file The interface defining a cart item, re-exported from Functions
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { CartExport } from 'functions/src/cart/exports/cart-export';
export { CartExport } from 'functions/src/cart/exports/cart-export';

/**
 * Cart item information needed to handle the item
 */
export type CartItem = CartExport & {
  /**
   * User ID of the user the cart belongs to.
   * @type string
   */
  uid: string;

  /**
   * The id of the cart item.
   * @type string
   */
  id: string;
};
