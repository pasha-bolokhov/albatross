/**
 * @file The component showing a cart item
 *
 * @author Pasha Bolokhov <pasha.bolokhov@gmail.com>
 */
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import {
  faTimes
} from '@fortawesome/free-solid-svg-icons';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CartItem } from './cart-item';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {
  @Input() item: CartItem;

  private ngOnDestroy$: Subject<any> = new Subject();

  /**
   * FontAwesome icons
   */
  faTimes = faTimes;

  constructor(private cartService: CartService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
    this.ngOnDestroy$.complete();
  }

  buy(): void {
    console.log(`%c Not implemented yet `, `color: white; background: violet`);       // AAAA
    this.snackBar.open(`Not implemented yet`, `Dismiss`, {duration: 2000, panelClass: 'not-implemented-yet-snack-bar'});
  }

  remove(): void {
    this.cartService.deleteCartItem(this.item).pipe(takeUntil(this.ngOnDestroy$)).subscribe();
  }
}
