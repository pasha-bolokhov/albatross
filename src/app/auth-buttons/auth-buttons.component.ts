import { BreakpointObserver } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { ViewChild } from '@angular/core';

import { LoginService } from '../auth/login.service';

@Component({
  selector: 'app-auth-buttons',
  templateUrl: './auth-buttons.component.html',
  styleUrls: ['./auth-buttons.component.scss']
})
export class AuthButtonsComponent {
  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;

  constructor (private breakpointObserver: BreakpointObserver, private loginService: LoginService) {
    breakpointObserver.observe([
      '(min-width: 600px)'
      ]).subscribe(() => {
        this.processMediaSizeChange();
    });
  }

  private processMediaSizeChange(): void {
    // Close the menu when the screen is larger then 'xs'
    if (this.menuTrigger?.menuOpen && this.breakpointObserver.isMatched('(min-width: 600px)')) {
      this.menuTrigger.closeMenu();
    }
  }

  signIn(): void {
    this.loginService.openSignInDialogue().subscribe();
  }

  signUp(): void {
    this.loginService.openSignUpDialogue();
  }
}
