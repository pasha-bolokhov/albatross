export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBJmEn_OsqBExDtiTglqKKIIH28XR1EQl4",
    authDomain: "albatross-travel-agency.firebaseapp.com",
    databaseURL: "https://albatross-travel-agency.firebaseio.com",
    projectId: "albatross-travel-agency",
    storageBucket: "albatross-travel-agency.appspot.com",
    messagingSenderId: "46781457201",
    appId: "1:46781457201:web:cc04f5b132be1b208ac610",
    measurementId: "G-1TSVTE0QQZ"
  }
};
